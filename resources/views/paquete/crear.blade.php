@extends('layouts.dashboard')
@section('content')
<div class="well p-b-50">
  <form method="POST" action="{{route('paquete.guardar')}}">
        {{ csrf_field() }}
        <div class="col-md-6">
        <div class="form-group p-t-0">
            <label class="f-15">N° paquete</label>
            <input type="text" class="form-control" name="n_paquete" placeholder="N° paquete" value="{{ old('n_paquete') }}">
            <span class="text-danger" >{{$errors->first('n_paquete')}}</span>
        </div>
       </div>
       <div class="col-md-6">
        <div class="form-group p-t-0">
            <label class="f-15">Remitente</label>
            <input type="text" class="form-control" name="remitente" placeholder="Remitente" value="{{ old('remitente') }}">
            <span class="text-danger" >{{$errors->first('remitente')}}</span>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-6">
        <div class="form-group p-t-0">
            <label class="f-15">N° rastreo</label>
            <input type="text" class="form-control" name="n_rastreo" placeholder="N° Rastreo" value="{{ old('n_rastreo') }}">
            <span class="text-danger" >{{$errors->first('n_rastreo')}}</span>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group p-t-0">
            <label class="f-15">Peso</label>
            <input type="text" class="form-control" name="peso" placeholder="Peso" value="{{ old('n_rastreo') }}">
            <span class="text-danger" >{{$errors->first('peso')}}</span>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-6">
        <div class="form-group p-t-0">
            <label class="f-15">Valor</label>
            <input type="text" class="form-control" name="valor" placeholder="Valor" value="{{ old('valor') }}">
            <span class="text-danger" >{{$errors->first('valor')}}</span>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group p-t-0">
            <label class="f-15">Cliente</label>
            {!! Form::select('cliente_id', $clientes, null, ['placeholder' => 'Seleccione...','class'=>'form-control']) !!}
            <span class="text-danger" >{{$errors->first('cliente_id')}}</span>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-6">
        <div class="form-group p-t-0">
            <label class="f-15">Contenido</label>
            <input type="text" class="form-control" name="contenido" placeholder="Contenido" value="{{ old('contenido') }}">
            <span class="text-danger" >{{$errors->first('contenido')}}</span>
        </div>
      </div>
      <div class="clearfix"></div>
      <hr>
        <div class="text-right">
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
   </form>
</div>
@endsection
