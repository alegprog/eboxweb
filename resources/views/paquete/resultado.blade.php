@extends('layouts.dashboard')
@section('content')
<div class="col-md-6">
  <div class="tile pt-inner">
      <div class="pti-header bg-cyan">
          <h2 class="f-25">Bs {{$costo}} <small></small></h2>
          <div class="ptih-title">Costo de paquete</div>
      </div>
  </div>
</div>
<div class="col-md-6">
  <div class="tile pt-inner">
      <div class="pti-header bg-cyan">
          <h2 class="f-25" >Bs {{$franqueo}} <small></small></h2>
          <div class="ptih-title">Franqueo Postal</div>
      </div>
  </div>
</div>
<div class="col-md-6">
<div class="tile pt-inner">
    <div class="pti-header bg-cyan">
        <h2 class="f-25" >Bs {{$seguro}} <small></small></h2>
        <div class="ptih-title">Seguro de paquete</div>
    </div>
</div>
</div>
<div class="col-md-6">
<div class="tile pt-inner">
    <div class="pti-header bg-green">
        <h2 class="f-25" >Bs {{$total}} <small></small></h2>
        <div class="ptih-title">Total a pagar</div>
    </div>
</div>
</div>
<h3>Detalle de Paquete</h3>
<div class="well p-b-50">
  <table class="table detalle">
    <tbody>
      <tr>
        <th width="25%" >N° paquete: </th>
        <td>{{$paquete->numero_paquete}}</td>
      </tr>
      <tr>
        <th>N° rastreo: </th>
        <td>{{$paquete->numero_rastreo}}</td>
      </tr>
      <tr>
        <th>Remitente: </th>
        <td>{{$paquete->remitente}}</td>
      </tr>
      <tr>
        <th>Peso: </th>
        <td>{{$paquete->peso}}kg.</td>
      </tr>
      <tr>
        <th>Valor: </th>
        <td>US${{$paquete->valor}}</td>
      </tr>
      <tr>
        <th>Contenido: </th>
        <td>{{$paquete->contenido}}</td>
      </tr>
    </tbody>
  </table>
</div>
@endsection
