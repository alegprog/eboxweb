@extends('layouts.dashboard')
@section('content')
<div class="well p-b-50">
  <form method="POST" action="{{route('paquete.guardarMovimiento')}}">
        {{ csrf_field() }}
        <div class="col-md-12">
        <div class="form-group p-t-0">
            <label class="f-15">Estatus</label>
            {!! Form::select('estatus', $estatus, null, ['placeholder' => 'Seleccione...','class'=>'form-control']) !!}
            <span class="text-danger" >{{$errors->first('estatus')}}</span>
        </div>
       </div>
       <div class="col-md-12">
        <div class="form-group p-t-0">
            <label class="f-15">Descripción</label>
            <textarea type="text" maxlength="250" class="form-control" name="descripcion" placeholder="Descripcion">{{ old('descripcion') }}</textarea>
            <span class="text-danger" >{{$errors->first('descripcion')}}</span>
        </div>
      </div>
      <div class="clearfix"></div>
      <hr>
      <b class="label label-success f-20">Estatus actual: {{$paquete->estatus}}</b>
      <div class="text-right">
        <input type="hidden" name="id" value="{{$paquete->id}}">
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
  </form>
</div>
<h3>Detalle de Paquete</h3>
<div class="well p-b-50">
  <table class="table detalle">
    <tbody>
      <tr>
        <th width="25%" >N° paquete: </th>
        <td>{{$paquete->numero_paquete}}</td>
      </tr>
      <tr>
        <th>N° rastreo: </th>
        <td>{{$paquete->numero_rastreo}}</td>
      </tr>
      <tr>
        <th>Remitente: </th>
        <td>{{$paquete->remitente}}</td>
      </tr>
      <tr>
        <th>Peso: </th>
        <td>{{$paquete->peso}}kg.</td>
      </tr>
      <tr>
        <th>Valor: </th>
        <td>US${{$paquete->valor}}</td>
      </tr>
      <tr>
        <th>Contenido: </th>
        <td>{{$paquete->contenido}}</td>
      </tr>
    </tbody>
  </table>
</div>
<h3>Movimientos</h3>
<div class="well p-b-50">
  <table class="table table-striped table-bordered detalle">
    <tbody>
      @foreach($paquete->movimientos as $movimiento)
      <tr>
        <th class="text-center" width="25%" > {{$movimiento->created_at->format('d-m-Y')}} | {{$movimiento->created_at->format('h:i A')}}</th>
        <td>{{$movimiento->descripcion}}</td>
        <th class="text-center">{{$movimiento->estatus}}</th>
      </tr>
      @endforeach
  </table>
</div>
@endsection
