@extends('layouts.dashboard')
@section('content')

<div class="well p-b-50">
  <form method="POST" action="{{route('costo.resultado')}}">
        {{ csrf_field() }}
        <div class="col-md-12">
        <div class="form-group p-t-0">
            <label class="f-15">N° paquete</label>
            {!! Form::select('n_paquete', $paquetes, null, ['placeholder' => 'Seleccione...','class'=>'form-control']) !!}
            <span class="text-danger" >{{$errors->first('n_paquete')}}</span>
        </div>
       </div>

      <div class="clearfix"></div>
      <hr>
        <div class="text-right">
          <button type="submit" class="btn btn-primary">Calcular costo</button>
        </div>
   </form>
</div>

@endsection
