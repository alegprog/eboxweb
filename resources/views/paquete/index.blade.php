@extends('layouts.dashboard')
@section('content')
    @if (session('mensaje_ok'))
         <div class="alert alert-success text-center">
             {{ session('mensaje_ok') }}
         </div>
    @endif
    @if (Auth::user()->tipo=='admin')
      <a class="btn btn-primary" href="{{route('paquete.registrar')}}">Registrar Paquete</a>
      <hr>
    @endif
    <table class="table text-center table-bordered">
      <thead>
        <tr>
          @if (Auth::user()->tipo=='admin')
            <th class="text-center" >Cliente</th>
          @endif
          <th class="text-center" >N° Paquete</th>
          <th class="text-center" >Contenido de paquete</th>
          <th class="text-center" >Estatu de paquete</th>
          <th class="text-center" >Acciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($paquetes as $paquete)
        <tr>
          @if (Auth::user()->tipo=='admin')
            <td>MCO{{$paquete->usuario_id}}</td>
          @endif
          <td>{{$paquete->numero_paquete}}</td>
          <td>{{$paquete->contenido}}</td>
          <td>{{$paquete->estatus}}</td>
          <td>
            <!-- Small button group -->
            <div class="btn-group">
              <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Opciones <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                @if (Auth::user()->tipo=='cliente')
                  <li><a href="{{route('paquete.detalle',['paquete'=>$paquete->numero_paquete])}}">Detalle</a></li>
                @endif
                @if (Auth::user()->tipo=='admin')
                  <li><a href="{{route('paquete.movimiento',['paquete'=>$paquete->numero_paquete])}}">Registrar Movimiento</a></li>
                @endif
              </ul>
            </div>
          </td>
        <tr>
      @endforeach
      </tbody>
    </table>

@endsection
