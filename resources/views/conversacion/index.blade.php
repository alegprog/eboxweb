@extends('layouts.dashboard')
@section('content')

@if (session('mensaje_ok'))
     <div class="alert alert-success text-center">
         {{ session('mensaje_ok') }}
     </div>
@endif

@if (Auth::user()->tipo=='cliente')
<a class="btn btn-primary" href="{{route('pregunta.registrar')}}">Registrar Pregunta</a>
<hr>
@endif

<table class="table text-center table-bordered">
  <thead>
    <tr>
      <th class="text-center" >Titulo de la pregunta</th>
      <th class="text-center" >Estatu</th>
      <th class="text-center" >Acciones</th>
    </tr>
  </thead>
  <tbody>
  @foreach($preguntas as $pregunta)
    <tr>
      <td>{{$pregunta->titulo}}</td>
      <td>{{$pregunta->estatus}}</td>
      <td>
        <!-- Small button group -->
        <div class="btn-group">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Opciones <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <li><a href="{{route('pregunta.detalle',['pregunta'=>$pregunta->id])}}">Detalle</a></li>
          </ul>
        </div>
      </td>
    <tr>
  @endforeach
  </tbody>
</table>

@endsection
