@extends('layouts.dashboard')
@section('content')
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading f-20"><b>Pregunta: {{$pregunta->titulo}}</b></div>
  <div class="panel-body">
    <p class="f-18">{{$pregunta->descripcion}}</p>
    <br>
    <div class="col-md-12 text-right"><b>Fecha publicacion: </b>{{$pregunta->created_at->format('j')}} de {{$pregunta->created_at->format('F Y h:i A')}}</div>
    <div class="col-md-12 text-right"><b>Autor: </b>{{$pregunta->autor->nombre}}, {{$pregunta->autor->apellido}}</div>
    <br><br>
    <hr>
    <p class="f-18 text-right"><b># Comentarios: ({{count($pregunta->comentarios)}})</b></p>
  </div>

  <!-- List group -->
  <div class="list-group">
    @foreach($pregunta->comentarios as $comentario)
    <a href="#" class="list-group-item">
      <h4 class="list-group-item-heading text-right f-15"><b>Autor:</b> {{$comentario->autor_tipo}}
        | <b>Fecha:</b> {{$comentario->created_at->format('d-m-Y h:i A')}}</h4>
        <br>
      <p class="list-group-item-text f-18"><b>Comentario:</b></p>
      <p class="p-l-30 p-r-30 text-justify f-18"> {{$comentario->mensaje}}</p>
      <br>
    </a>
    @endforeach
  </div>
</div>
<div class="well p-b-50">
  <form method="POST" action="{{route('pregunta.comentario')}}">
      {{ csrf_field() }}
      <div class="col-md-12">
        <div class="form-group p-t-0">
            <label class="f-15">Comentario</label>
            <textarea type="text" maxlength="250" class="form-control" name="comentario" placeholder="Comentario">{{ old('comentario') }}</textarea>
            <span class="text-danger" >{{$errors->first('comentario')}}</span>
        </div>
      </div>
      <div class="clearfix"></div>
      <hr>
      <div class="text-right">
        <input type="hidden" name="id" value="{{$pregunta->id}}">
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
  </form>
</div>
@endsection
