@extends('layouts.dashboard')
@section('content')
<div class="well p-b-50">
  <form method="POST" action="{{route('pregunta.guardar')}}">
        {{ csrf_field() }}
        <div class="col-md-12">
        <div class="form-group p-t-0">
            <label class="f-15">Preguta</label>
            <input type="text" class="form-control" name="pregunta" placeholder="Pregunta" value="{{ old('pregunta') }}">
            <span class="text-danger" >{{$errors->first('pregunta')}}</span>
        </div>
       </div>
       <div class="col-md-12">
        <div class="form-group p-t-0">
            <label class="f-15">Descripción</label>
            <textarea type="text" class="form-control" name="descripcion" placeholder="Descripcion">{{ old('descripcion') }}</textarea>
            <span class="text-danger" >{{$errors->first('descripcion')}}</span>
        </div>
      </div>
      <div class="clearfix"></div>
      <hr>
      <div class="text-right">
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
  </form>
</div>
@endsection
