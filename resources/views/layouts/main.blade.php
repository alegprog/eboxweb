<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>EboxWeb</title>

    <!-- Bootstrap -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <div id="wrapper">
    <div class="header">
      <div class="container">
        <div class="row">
      <div class="block">
        <div class="logo">
          <a href="{{url('/')}}"><img src="{{asset('assets/img/logo.png')}}" galleryimg="NO"></a>
        </div>
        <div class="menu">
          <div class="nav">
          <ul>
          <li><a href="{{url('/')}}" class="active">Mi e-box</a></li>
          <!--<li><a href="http://www.mbelatam.com/">&lt; Volver al nuestro sitio web</a></li></ul>-->

          <!--<ul>
          <li><a href="https://mbe-latam.com/eboxweb/"  >Inicio</a></li>
          <li><a href="https://mbe-latam.com/eboxweb/contact.html" >Contactenos</a></li>
          </ul>-->
          </div>
        </div>

      <div class="clear"></div>
     </div>
    </div>
  </div>
</div>
</div>

<div class="container">
 <div class="row">
   <div class="col-md-12">
    @yield('content')
  </div>
  </div>
 </div>
<div>


    <script src="{{asset('assets/js/jquery.min.js')}}" ></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}" ></script>
  </body>
</html>
