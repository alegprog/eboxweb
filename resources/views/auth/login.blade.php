@extends('layouts.main')
@section('content')
        <div class="row">
            <br><br>
            <div class="col-md-offset-2 col-md-8">
              @if (Session::has('errors'))
              <div class="alert alert-warning" role="alert">
                <strong>Ups! Algo salió mal : </strong>
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
              </div>
              @endif

              @if (Session::has('mensaje'))
              <div class="alert alert-warning" role="alert">
                <strong>Ups! Algo salió mal : </strong>
                <ul>
                      <li>{{ Session::get('mensaje') }}</li>
                </ul>
              </div>
              @endif

              @if (session('mensaje_ok'))
              <div class="alert alert-success text-center">
                  {{ session('mensaje_ok') }}
              </div>
              @endif
            </div>
            <div class="col-md-4 col-md-offset-2">

                <h2 class="text-center f-20">Inicio de Sesión</h2>
                <div class="well">

                      <form method="POST" action="auth/login">
                            {{ csrf_field() }}
                            <br>
                            <div class="form-group">
                                <label class="f-15">Correo</label>
                                <input type="text" class="form-control" autocomplete="false" name="correo" placeholder="Correo">
                            </div>
                            <div class="form-group">
                                <label class="f-15" >Contraseña</label>
                                <input type="password" class="form-control" autocomplete="false" name="clave" placeholder="Contraseña">
                            </div>
                            <div>
                              <button type="submit" class="btn btn-primary ">Iniciar Sesión</button>
                            </div>
                            <hr>

                     </form>
                   </div>
            </div>
            <div class="col-md-4">
              <br><br><br><br><br>
              <h3>Aun no te encuentras registrado?</h3>
              <div class="well">
                <a href="{{route('registro.create')}}" class="btn btn-warning">Registrarme</a>
              </div>
            <div>
        </div>

@endsection
