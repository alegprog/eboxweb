@extends('layouts.main')
@section('content')
    <br><br>
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('login')}}" class="btn btn-warning">Volver</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Registrame</h2>
          <div class="well col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('registro.store')}}">
                  {{ csrf_field() }}
                  <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Nombre</label>
                      <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{ old('nombre') }}">
                      <span class="text-danger" >{{$errors->first('nombre')}}</span>
                  </div>
                 </div>
                 <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Apellido</label>
                      <input type="text" class="form-control" name="apellido" placeholder="Apellido" value="{{ old('apellido') }}">
                      <span class="text-danger" >{{$errors->first('apellido')}}</span>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Contraseña</label>
                      <input type="password" class="form-control" name="clave" placeholder="Contraseña" value="">
                      <span class="text-danger" >{{$errors->first('clave')}}</span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Confirmar contraseña</label>
                      <input type="password" class="form-control" name="clave_confirmation" placeholder="Confirmar contraseña" value="">
                      <span class="text-danger" >{{$errors->first('clave_confirmation')}}</span>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Correo</label>
                      <input type="text" class="form-control" name="correo" placeholder="Correo" value="{{ old('correo') }}">
                      <span class="text-danger" >{{$errors->first('correo')}}</span>
                  </div>
                </div>
                <div class="clearfix"></div>
                  <div class="text-right">
                    <button type="submit" class="btn btn-primary">Registrate</button>
                  </div>
             </form>
          </div>

        </div>
    </div>
@endsection
