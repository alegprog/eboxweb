@extends('layouts.dashboard')
@section('content')




   <div class="carousel slide" id="carousel-example-generic" data-ride="carousel">
     <ol class="carousel-indicators">
       <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
       <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
       <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
       <li data-target="#carousel-example-generic" data-slide-to="3" class=""></li>
       <li data-target="#carousel-example-generic" data-slide-to="4" class=""></li>
     </ol>
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img alt="First slide [900x500]" class="img-responsive" width="100%" data-src="holder.js/900x500/auto/#777:#555/text:First slide"
        src="{{asset('assets/img/MBE-navidad.jpg')}}" data-holder-rendered="true">
      </div>
      <div class="item">
        <img alt="First slide [900x500]" class="img-responsive" width="100%"  data-src="holder.js/900x500/auto/#777:#555/text:First slide"
        src="{{asset('assets/img/mbe.png')}}" data-holder-rendered="true">
      </div>
      <div class="item">
        <img alt="First slide [900x500]" data-src="holder.js/900x500/auto/#777:#555/text:First slide"
        src="{{asset('assets/img/empaca-MBE.jpg')}}" data-holder-rendered="true">
      </div>
      <div class="item">
        <img alt="Second slide [900x500]" data-src="holder.js/900x500/auto/#666:#444/text:Second slide"
        src="{{asset('assets/img/website-documentos.jpg')}}" data-holder-rendered="true">
      </div>
      <div class="item">
        <img alt="Third slide [900x500]" data-src="holder.js/900x500/auto/#555:#333/text:Third slide"
        src="{{asset('assets/img/mbe-cajas.jpg')}}" data-holder-rendered="true">
      </div>
    </div>
    <a href="#carousel-example-generic" class="left carousel-control" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Anterior</span> </a>
      <a href="#carousel-example-generic" class="right carousel-control" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Siguientes</span>
      </a>
    </div>

@endsection
