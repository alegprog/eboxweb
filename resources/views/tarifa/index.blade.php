@extends('layouts.dashboard')
@section('content')
@if (session('mensaje_ok'))
     <div class="alert alert-success text-center">
         {{ session('mensaje_ok') }}
     </div>
@endif
@if (Auth::user()->tipo=='admin')
<a href="{{route('tarifa.editar')}}" class="btn btn-primary" >Editar tarifa</a>
<hr>
@endif
<div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                <strong>Información sobre tarifas</strong>
              </a>
            </h4>
          </div>
          <div class="panel-body gris">
            <div class="col-sm-12">
            <div class="tile pt-inner">
                <div class="pti-header bg-green">
                    <h2>Bs {{number_format($tarifa->valor, 2, ',', '.')}} <small></small></h2>
                    <div class="ptih-title">Tarifa de envio</div>
                </div>

                <div class="pti-body">
                    <div class="ptib-item">
                        <strong><i class="glyphicon glyphicon-menu-right"></i> Descripción:</strong>
                        {{$tarifa->descripcion}}
                    </div>
                    <div class="ptib-item">
                        <strong><i class="glyphicon glyphicon-menu-right"></i> Calculo:</strong>
                        {{$tarifa->calculo}}
                    </div>

                </div>

                <div class="pti-footer">

                </div>
            </div>
        </div>
          </div>
    </div>


@endsection
