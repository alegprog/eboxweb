@extends('layouts.dashboard')
@section('content')
<div class="well p-b-50">
  <form method="POST" action="{{route('tarifa.actualizar')}}">
        {{ csrf_field() }}
        <div class="col-md-6">
        <div class="form-group p-t-0">
            <label class="f-15">Descripcion</label>
            <textarea class="form-control" name="descripcion" placeholder="Descripcion">{{ old('descripcion') ? old('descripcion') : $tarifa->descripcion }}</textarea>
            <span class="text-danger" >{{$errors->first('descripcion')}}</span>
        </div>
       </div>
       <div class="col-md-6">
        <div class="form-group p-t-0">
            <label class="f-15">Calculo</label>
            <textarea maxlength="250" class="form-control" name="calculo" placeholder="Calculo">{{ old('calculo') ? old('calculo') : $tarifa->calculo }}</textarea>
            <span class="text-danger" >{{$errors->first('calculo')}}</span>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-6">
        <div class="form-group p-t-0">
            <label class="f-15">Valor</label>
            <input type="text" class="form-control" name="valor" placeholder="Valor" value="{{ old('valor') ? old('valor') : $tarifa->valor }}">
            <span class="text-danger" >{{$errors->first('valor')}}</span>
        </div>
      </div>

      <div class="clearfix"></div>
      <hr>
        <div class="text-right">
          <button type="submit" class="btn btn-primary">Actualizar</button>
        </div>
   </form>
</div>
@endsection
