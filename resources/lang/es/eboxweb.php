<?php

return array(

    'dashboard_title' => 'Inicio',
    'tarifa_title' => 'Tarifa',
    'tarifa.editar_title' => 'Editar tarifa',
    'paquete_title' => 'Mis paquetes',
    'clientes_title' => 'Clientes paquetes',
    'paquete.registrar_title' => 'Registrar paquete',
    'paquete.detalle_title' => 'Detalle del paquete',
    'paquete.movimiento_title' => 'Registrar movimiento',
    'preguntas_title' => 'Conversaciones',
    'pregunta.registrar_title'=>'Registrar preguntas o sugerencias',
    'pregunta.detalle_title'=>'Detalle conversación',
    'costo_title' => 'Costo de paquete',
    'costo.resultado_title' => 'Costo del paquete',

);
