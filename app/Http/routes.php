<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('login', ['as' =>'login', 'uses' => 'DashboardController@getLogin']);
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'DashboardController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'DashboardController@getLogout']);

Route::get('registro', ['as' =>'registro.create', 'uses' => 'DashboardController@getRegistro']);
Route::post('registro', ['as' =>'registro.store', 'uses' => 'DashboardController@postRegistro']);

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::group(['middleware' => 'auth'], function () {

  Route::get('dashboard', [
      'as' => 'dashboard', 'uses' => 'DashboardController@index'
  ]);

  Route::get('dashboard/mis-paquete', [
      'as' => 'paquete', 'uses' => 'PaqueteController@index'
  ]);

  Route::get('dashboard/cliente-paquete', [
      'as' => 'clientes', 'uses' => 'PaqueteController@index'
  ]);

  Route::get('dashboard/paquete/registrar', [
      'as' => 'paquete.registrar', 'uses' => 'PaqueteController@registrar'
  ]);

  Route::post('dashboard/paquete/guardar', [
      'as' => 'paquete.guardar', 'uses' => 'PaqueteController@guardar'
  ]);

  Route::get('dashboard/paquete/detalle/{paquete}', [
      'as' => 'paquete.detalle', 'uses' => 'PaqueteController@detalle'
  ]);

  Route::get('dashboard/paquete/movimiento/{paquete}', [
      'as' => 'paquete.movimiento', 'uses' => 'PaqueteController@movimiento'
  ]);

  Route::post('dashboard/movimiento/guardar/', [
      'as' => 'paquete.guardarMovimiento', 'uses' => 'PaqueteController@guardarMovimiento'
  ]);

  Route::get('dashboard/tarifa', [
      'as' => 'tarifa', 'uses' => 'TarifaController@index'
  ]);

  Route::get('dashboard/tarifa/editar', [
      'as' => 'tarifa.editar', 'uses' => 'TarifaController@editar'
  ]);

  Route::post('dashboard/tarifa/actualizar/', [
      'as' => 'tarifa.actualizar', 'uses' => 'TarifaController@actualizar'
  ]);

  Route::get('dashboard/preguntas', [
      'as' => 'preguntas', 'uses' => 'PreguntaController@index'
  ]);

  Route::get('dashboard/pregunta/registrar', [
      'as' => 'pregunta.registrar', 'uses' => 'PreguntaController@registrar'
  ]);

  Route::post('dashboard/pregunta/guardar', [
      'as' => 'pregunta.guardar', 'uses' => 'PreguntaController@guardar'
  ]);

  Route::get('dashboard/pregunta/detalle/{id}', [
      'as' => 'pregunta.detalle', 'uses' => 'PreguntaController@detalle'
  ]);

  Route::post('dashboard/pregunta/contestar', [
      'as' => 'pregunta.comentario', 'uses' => 'PreguntaController@comentario'
  ]);

  Route::get('dashboard/calcular-costo', [
      'as' => 'costo', 'uses' => 'PaqueteController@calcularCosto'
  ]);

  Route::post('dashboard/resultado-costo', [
      'as' => 'costo.resultado', 'uses' => 'PaqueteController@resultadoCosto'
  ]);

});

/*Route::get('dashboard/tarifa', function () {
    return view('tarifa.index');
});

Route::get('dashboard/conversacion', function () {
    return view('conversacion.index');
});*/
