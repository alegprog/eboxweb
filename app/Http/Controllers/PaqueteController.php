<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuario;
use App\Paquete;
use App\Movimiento;
use App\Tarifa;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PaqueteController extends Controller
{
  public function index()
  {
      if(auth()->user()->tipo=='cliente'){
        $paquetes=Paquete::withTrashed()->where('usuario_id',auth()->user()->id)->get();
      }else{
        $paquetes=Paquete::withTrashed()->get();
      }

      return view('paquete.index',compact('paquetes'));
  }

  public function registrar()
  {
      //$usuarios=Usuario::select();
      $clientes=Usuario::select('id',DB::raw("concat('MCO',id,' - ',nombre,' ',apellido) as nombre"))
      ->where('tipo','cliente')
      ->lists('nombre','id');
      //dd($usuarios);
      return view('paquete.crear',compact('clientes'));
  }

  public function guardar(Request $request){
    $this->validate($request, [
      'n_paquete'=> 'required|unique:paquetes,numero_paquete|alpha_num',
      'n_rastreo'=> 'required|unique:paquetes,numero_rastreo|alpha_num',
      'remitente'=> 'required',
      'peso'=> 'required|numeric',
      'valor'=> 'required|numeric',
      'cliente_id'=> 'required',
      'contenido'=> 'required'
    ]);

    //dd($request);

    $paquete= new Paquete();
    $paquete->numero_paquete=$request->n_paquete;
    $paquete->numero_rastreo=$request->n_rastreo;
    $paquete->remitente=$request->remitente;
    $paquete->peso=$request->peso;
    $paquete->valor=$request->valor;
    $paquete->usuario_id=$request->cliente_id;
    $paquete->contenido=$request->contenido;
    $paquete->estatus='Procesando en Instalaciones de Miami';
    $paquete->save();

    $movimiento=new Movimiento();
    $movimiento->estatus='Procesando en Instalaciones de Miami';
    $movimiento->descripcion="El paquete ha sido recibido en las instalaciones de miami fue registrado y procesado";
    $movimiento->paquete_id=$paquete->id;
    $movimiento->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('paquete')->with('mensaje_ok',$ok);
  }

  public function detalle($paquete)
  {
    $paquete=Paquete::withTrashed()->with(['cliente' => function ($query) {
          $query->withTrashed();
        },'movimientos' => function ($query) {
          $query->withTrashed()->orderBy('created_at', 'desc');
        }
    ])->where('numero_paquete', $paquete)->first();
    //dd($paquete);

    return view('paquete.detalle',compact('paquete'));
  }

  public function movimiento($paquete)
  {
    $paquete=Paquete::withTrashed()->with(['cliente' => function ($query) {
          $query->withTrashed();
        },'movimientos' => function ($query) {
          $query->withTrashed()->orderBy('created_at', 'desc');
        }
    ])->where('numero_paquete', $paquete)->first();

    $estatus=[
      'Paquete Listo Para Salir De Miami'=>'Paquete Listo Para Salir De Miami',
      'En Transito'=>'En Transito',
      'Aduanas'=>'Aduanas',
      'Mercancia En Revision'=>'Mercancia En Revision',
      'Disponible En Centro MBE'=>'Disponible En Centro MBE',
      'Retirado Por El Cliente'=>'Retirado Por El Cliente',      
      'Entregado A Un Tercero'=>'Entregado A Un Tercero'
    ];

    return view('paquete.movimiento',compact('paquete','estatus'));
  }

  public function guardarMovimiento(Request $request){
    $this->validate($request, [
      'estatus'=> 'required',
      'descripcion'=> 'required|min:30'
    ]);

    $movimiento=new Movimiento();
    $movimiento->estatus=$request->estatus;
    $movimiento->descripcion=$request->descripcion;
    $movimiento->paquete_id=$request->id;
    $movimiento->save();

    $paquete=Paquete::withTrashed()->find($request->id);
    $paquete->estatus=$request->estatus;
    $paquete->save();

    $ok='Excelente! se ha registrado el movimiento satisfactoriamente';
    return redirect()->route('paquete')->with('mensaje_ok',$ok);
  }




  public function calcularCosto()
  {
    if(auth()->user()->tipo=='cliente'){
      $paquetes=Paquete::withTrashed()->select('id',DB::raw("concat('N° Paquete: ',numero_paquete,' - ',contenido) as nombre"))
      ->where('usuario_id',auth()->user()->id)
      ->lists('nombre','id');
    }else{
      $paquetes=Paquete::withTrashed()->select('id',DB::raw("concat('N° Paquete: ',numero_paquete,' - ',contenido) as nombre"))
      ->lists('nombre','id');
    }
      //dd($usuarios);
      return view('paquete.costo',compact('paquetes'));
  }

  public function resultadoCosto(Request $request){
    $this->validate($request, [
      'n_paquete'=> 'required',
    ]);

    $paquete=Paquete::withTrashed()->find($request->n_paquete);
    $tarifa=Tarifa::withTrashed()->first();
    if($tarifa==null){
      $warning='No se puede calcular el costo del paquete no se encuentra la tarifa';
      return redirect()->route('costo')->with('mensaje_warning',$warning);
    }
    //dd($paquete);
    $costo=number_format($paquete->peso*$tarifa->valor, 2, ',', '.');
    $gramos=($paquete->peso/0.0010000);

    if($gramos<=500){
      $porcentaje=($paquete->peso*$tarifa->valor)*0.08;
      $franqueo=number_format($porcentaje, 2, ',', '.');
    }elseif($gramos>=501 && $gramos<=1000){
      $porcentaje=($paquete->peso*$tarifa->valor)*0.07;
      $franqueo=number_format($porcentaje, 2, ',', '.');
    }elseif($gramos>=1001 && $gramos<=2000){
      $porcentaje=($paquete->peso*$tarifa->valor)*0.06;
      $franqueo=number_format($porcentaje, 2, ',', '.');
    }else{
      $franqueo=number_format(0.00, 2, ',', '.');
    }

    $seguro=number_format(1226.40, 2, ',', '.');
    $total=number_format(($paquete->peso*$tarifa->valor)+$porcentaje+1226.40, 2, ',', '.');

    $ok='Excelente! se ha calculado el costo del paquete satisfactoriamente';
    return view('paquete.resultado',compact('paquete','costo','franqueo','seguro','total'));

  }
}
