<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tarifa;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TarifaController extends Controller
{

    public function index()
    {
      $tarifa=Tarifa::withTrashed()->first();
      if($tarifa==null){
        $tarifa=(object)['descripcion'=>'','calculo'=>'','valor'=>''];        
      }
        return view('tarifa.index',compact('tarifa'));
    }

    public function editar()
    {
        $tarifa=Tarifa::withTrashed()->first();
        if($tarifa==null){
          //dd('hola');
          $tarifa=(object)['descripcion'=>'','calculo'=>'','valor'=>''];
          //$tarifa=get_object_vars($tarifa_array);
          //$tarifa=collect($tarifa_array);
          //dd($tarifa);
        }
        return view('tarifa.editar',compact('tarifa'));
    }

    public function actualizar(Request $request){
      $this->validate($request, [
        'descripcion'=> 'required|min:30',
        'calculo'=> 'required|min:30',
        'valor'=> 'required|numeric'
      ]);
      $tarifaBuscar=Tarifa::withTrashed()->first();
      if($tarifaBuscar==null){
        $tarifa=new Tarifa();
        $tarifa->descripcion=$request->descripcion;
        $tarifa->calculo=$request->calculo;
        $tarifa->valor=$request->valor;
        $tarifa->save();
      }else{
        $tarifa=Tarifa::withTrashed()->find($tarifaBuscar->id);
        $tarifa->descripcion=$request->descripcion;
        $tarifa->calculo=$request->calculo;
        $tarifa->valor=$request->valor;
        $tarifa->save();
      }

      $ok='Excelente! se ha actualizado la tarifa satisfactoriamente';
      return redirect()->route('tarifa')->with('mensaje_ok',$ok);
    }


}
