<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Pregunta;
use App\Comentario;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PreguntaController extends Controller
{

  public function index()
  {
      if(auth()->user()->tipo=='cliente'){
        $preguntas=Pregunta::withTrashed()->where('usuario_id',auth()->user()->id)->get();
      }else{
        $preguntas=Pregunta::withTrashed()->get();
      }

      //dd($preguntas);

      return view('conversacion.index',compact('preguntas'));
  }

  public function registrar()
  {
      return view('conversacion.crear');
  }

  public function guardar(Request $request){
    $this->validate($request, [
      'pregunta'=>'required',
      'descripcion'=> 'required|min:30',
    ]);

    $pregunta=new Pregunta();
    $pregunta->estatus='Sin contestar';
    $pregunta->descripcion=$request->descripcion;
    $pregunta->titulo=$request->pregunta;
    $pregunta->usuario_id=auth()->user()->id;
    $pregunta->save();

    $ok='Excelente! se ha registrado la pregunta satisfactoriamente';
    return redirect()->route('preguntas')->with('mensaje_ok',$ok);
  }

  public function detalle($id)
  {
    $pregunta=Pregunta::withTrashed()->with(['autor' => function ($query) {
          $query->withTrashed();
        },'comentarios' => function ($query) {
          $query->withTrashed();
          $query->with(['autor' => function ($query) {
            $query->withTrashed();
          }]);
        }
    ])->where('id', $id)->first();
    /*$pregunta=Pregunta::withTrashed()->where('id', $id)->first();*/
    //dd($pregunta);

    return view('conversacion.detalle',compact('pregunta'));
  }

  public function comentario(Request $request){
    $this->validate($request, [
      'comentario'=> 'required|min:5',
    ]);

    $comentario=new Comentario();
    //$comentario->estatus='Sin contestar';
    $comentario->mensaje=$request->comentario;
    $comentario->usuario_id=auth()->user()->id;
    $comentario->pregunta_id=$request->id;
    $comentario->save();

    $pregunta=Pregunta::withTrashed()->where('id',$request->id)->first();
    if(auth()->user()->tipo=='cliente'){
      $pregunta->estatus='Sin contestar';
    }else{
      $pregunta->estatus='Respondida';
    }
    $pregunta->save();

    $ok='Excelente! se ha registrado un nuevo comentario satisfactoriamente';
    return redirect()->route('preguntas')->with('mensaje_ok',$ok);
  }

}
