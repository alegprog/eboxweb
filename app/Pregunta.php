<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Date\Date;

class Pregunta extends Model
{
    use SoftDeletes;

    public function autor()
    {
        return $this->belongsTo('App\Usuario','usuario_id','id');
    }

    public function comentarios()
    {
         return $this->hasMany('App\Comentario');
    }

    public function getCreatedAtAttribute($value){
      return new Date($value);
      //return new Date($value, new DateTimeZone('Americas/Caracas'));
    }

    public function getAutorTipoAttribute(){
      if($this->autor()->tipo=='cliente'){
         return 'Comentario del (Cliente):'.' '.$this->autor()->nombre.', '.$this->autor()->apellido;
      }else{
         return 'Comentario del (Administrador):'.' '.$this->autor()->nombre.', '.$this->autor()->apellido;
      }
    }
}
