<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Date\Date;

class Comentario extends Model
{
  use SoftDeletes;

  public function autor()
  {
      return $this->belongsTo('App\Usuario','usuario_id','id');
  }

  public function getCreatedAtAttribute($value){
    return new Date($value);
  }

  public function getAutorTipoAttribute(){
    if($this->autor->tipo=='cliente'){
       return '(Cliente):'.' '.$this->autor->nombre.', '.$this->autor->apellido;
    }else{
       return '(Administrador):'.' '.$this->autor->nombre.', '.$this->autor->apellido;
    }
  }
}
