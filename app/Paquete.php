<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Paquete extends Model
{
  use SoftDeletes;

  public function cliente()
  {
      return $this->belongsTo('App\Usuario','usuario_id','id');
  }

  public function movimientos()
  {
       return $this->hasMany('App\Movimiento');
  }

}
